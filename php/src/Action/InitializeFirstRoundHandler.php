<?php

namespace App\Action;

use App\Domain\Deck;
use App\Domain\Player;
use App\Domain\Round;
use Doctrine\ORM\EntityManagerInterface;
use Ramsey\Uuid\Uuid;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class InitializeFirstRoundHandler implements MessageHandlerInterface
{
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function __invoke(InitializeFirstRound $command)
    {
        $gameID = Uuid::uuid4();

        $players = [];
        for ($i = 0; $i < 4; $i++) {
            $player = new Player($gameID, $i, $i === 0);
            $players[] = $player;
            $this->entityManager->persist($player);
        }

        $deck = Deck::createNew();

        $round = new Round($gameID, $players);

        $round->dispatchStartingGold();
        $round->getStartingHand($deck);

        $this->entityManager->persist($round);
        $this->entityManager->flush();

        return $round;
    }
}