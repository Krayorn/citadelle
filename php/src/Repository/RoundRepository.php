<?php

declare(strict_types=1);

namespace App\Repository;

use App\Domain\Round;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class RoundRepository extends ServiceEntityRepository
{
    private PlayerRepository $playerRepository;

    public function __construct(
        ManagerRegistry $registry,
        PlayerRepository $playerRepository
    )
    {
        $this->playerRepository = $playerRepository;
        parent::__construct($registry, Round::class);
    }

    public function findById(int $roundId): Round
    {
        $round = $this->find($roundId);

        $players = $this->playerRepository->findAllByGameId($round->getGameId());

        $round->setPlayers($players);

        return $round;
    }
}