<?php

declare(strict_types=1);

namespace App\Repository;

use App\Domain\Deck;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Ramsey\Uuid\UuidInterface;

class DeckRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Deck::class);
    }

    public function getDeck(UuidInterface $gameId)
    {
        $conn = $this->getEntityManager()->getConnection();

        $sql = '
            SELECT discard.name, discard.cost, discard.kind 
            FROM discard
            WHERE 
                game_id = :gameId
        ';

        $stmt = $conn->prepare($sql);
        $result = $stmt->executeQuery(['gameId' => $gameId])->fetchAllAssociativeIndexed();
        dd($result);
        return $result;
    }

}