<?php

declare(strict_types=1);

namespace App\Repository;

use App\Domain\Player;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Ramsey\Uuid\UuidInterface;

class PlayerRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Player::class);
    }

    public function findAllByGameId(UuidInterface $gameId): array
    {
        return $this->findBy(['gameId' => $gameId]);
    }
}