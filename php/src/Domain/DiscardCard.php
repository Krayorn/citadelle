<?php

declare(strict_types=1);

namespace App\Domain;

use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\UuidInterface;

/**
 * @ORM\Entity()
 * @ORM\Table(name="discard")
 */
class DiscardCard extends District
{
    /**
     * @ORM\Column(type="uuid")
     * @ORM\Id()
     */
    private UuidInterface $gameId;

    public function __construct(UuidInterface $uuid, District $district)
    {
        $this->gameId = $uuid;

        parent::__construct($district->getName(), $district->getCost(), $district->getKind());
    }
}