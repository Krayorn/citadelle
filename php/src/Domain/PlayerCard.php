<?php

declare(strict_types=1);

namespace App\Domain;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="player_cards")
 */
class PlayerCard extends District
{
    /**
     * Many DeckCard have one Deck
     * @ORM\Id()
     * @ORM\ManyToOne(targetEntity="Player", inversedBy="hand")
     * @ORM\JoinColumn(name="player_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private Player $player;

    public function __construct(Player $player, District $district)
    {
        $this->player = $player;

        parent::__construct($district->getName(), $district->getCost(), $district->getKind());
    }
}