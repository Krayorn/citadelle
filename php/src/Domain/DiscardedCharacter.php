<?php

declare(strict_types=1);

namespace App\Domain;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="discarded_characters")
 */
class DiscardedCharacter
{
    /**
     * Many DeckCard have one Deck
     * @ORM\Id()
     * @ORM\ManyToOne(targetEntity="Round", inversedBy="discardedCharacters")
     * @ORM\JoinColumn(name="round_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private Round $round;

    /**
     * @ORM\Column(type="string")
     * @ORM\Id()
     */
    private string $character;

    /**
     * @ORM\Column(type="boolean")
     */
    private bool $visible;

    public function __construct(Round $round, string $character, bool $visible)
    {
        $this->round = $round;
        $this->character = $character;
        $this->visible = $visible;
    }

    public function getCharacter(): string
    {
        return $this->character;
    }

    public function isVisible(): bool
    {
        return $this->visible;
    }
}