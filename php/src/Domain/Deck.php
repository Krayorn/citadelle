<?php

declare(strict_types=1);

namespace App\Domain;

class Deck
{
    private array $stack;

    public function __construct(array $discard, array $playerHands, array $playerCities)
    {
        $cards = [];
        $cards[] = new District('Quartier Marchant', 2, District::KIND_MERCHANT);
        $cards[] = new District('Quartier Marchant', 2, District::KIND_MERCHANT);
        $cards[] = new District('Eglise', 1, District::KIND_PRIEST);
        $cards[] = new District('Palais', 4, District::KIND_KING);
        $cards[] = new District('Tour de garde', 1, District::KIND_CONDOTIERRE);
        $cards[] = new District('Garnison', 3, District::KIND_CONDOTIERRE);

        shuffle($cards);

        $this->stack = $cards;
    }

    static function createNew(): Deck
    {
        return new Deck([], [], []);
    }

    public function getNextCard(): District
    {
        return array_pop($this->stack);
    }
}