<?php

declare(strict_types=1);

namespace App\Domain;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\UuidInterface;

/**
 * @ORM\Entity()
 * @ORM\Table(name="rounds")
 */
class Round
{
    CONST characters = [
        'assassin',
        'thief',
        'mage',
        'king',
        'cleric',
        'merchant',
        'architect',
        'condotierre'
    ];

    /**
     * @ORM\Id()
     * @ORM\Column(type="integer", unique=true)
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private int $id;

    /**
     * @ORM\Column(type="uuid")
     */
    private UuidInterface $gameId;

    /**
     * @ORM\Column(type="integer")
     */
    private int $turn;

    /**
     * One Player has many Cards.
     * @ORM\OneToMany(targetEntity="DiscardedCharacter", cascade={"persist"}, mappedBy="round")
     */
    private Collection $discardedCharacters;

    private array $players;
    private array $initiative;

    public function __construct(UuidInterface $uuid, array $players)
    {
        $this->gameId = $uuid;
        $this->players = $players;
        $this->turn = 0;
        $this->discardedCharacters = new ArrayCollection();

        $this->decideInitiative();

        $characters = self::characters;
        shuffle($characters);

        $this->discardedCharacters->add(new DiscardedCharacter($this, $characters[0], false));
        // TODO: King can't be discarded visible
        $this->discardedCharacters->add(new DiscardedCharacter($this, $characters[1], true));
        $this->discardedCharacters->add(new DiscardedCharacter($this, $characters[2], true));
    }

    public function dispatchStartingGold()
    {
        foreach ($this->players as $player) {
            $player->earnGold(2);
        }
    }

    public function getStartingHand(Deck $deck)
    {
        foreach ($this->players as $player) {
            $player->drawCard($deck);
        }
    }

    public function canPlayerPick(int $pickerPlayerId): bool
    {
        if ($this->turn !== 0) {
            return false;
        }

        foreach ($this->initiative as $playerId) {
            if ($pickerPlayerId === $playerId) {
                return $this->players[$playerId]->getCharacter() === null;
            }

            if ($this->players[$playerId]->getCharacter() === null) {
                return false;
            }
        }

        return true;
    }

    public function setPlayers(array $players): void
    {
        foreach ($players as $player) {
            $this->players[$player->getId()] = $player;
        }
        $this->decideInitiative();
    }

    public function getGameId(): UuidInterface
    {
        return $this->gameId;
    }

    private function decideInitiative()
    {
        ksort($this->players);

        $initiativeOrder = [];
        $lastPlayers = [];
        $firstPlayer = false;
        foreach ($this->players as $order => $player) {
            if ($player->isCrowned()) {
                $firstPlayer = true;
            }
            if ($firstPlayer) {
                $initiativeOrder[] = $player->getId();
            } else {
                $lastPlayers[] = $player->getId();
            }
        }

        $this->initiative = array_merge($initiativeOrder, $lastPlayers);
    }

    public function getAvailableCharacters(): array
    {
        $characters = [];
        foreach (self::characters as $character) {
            $characters[$character] = true;
        }

        foreach ($this->players as $player) {
            if ($player->getCharacter() !== null) {
                $characters[$player->getCharacter()] = false;
            }
        }

        foreach ($this->discardedCharacters as $discardedCharacter) {
            $characters[$discardedCharacter->getCharacter()] = false;
        }

        return array_keys(array_filter($characters, fn($character) => $character));
    }

    public function getVisibleDiscardedCharacters(): array
    {
        $visibleDiscardedCharacters = [];

        foreach ($this->discardedCharacters as $discardedCharacter) {
            if ($discardedCharacter->isVisible()) {
                $visibleDiscardedCharacters[] = $discardedCharacter->getCharacter();
            }
        }

        return $visibleDiscardedCharacters;
    }
}