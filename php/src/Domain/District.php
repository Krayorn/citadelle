<?php

declare(strict_types=1);

namespace App\Domain;

use Exception;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\MappedSuperclass
 */
class District
{
    const KIND_MERCHANT = 'green';
    const KIND_KING = 'yellow';
    const KIND_CONDOTIERRE = 'red';
    const KIND_PRIEST = 'blue';

    const ALLOWED_KINDS = [
        self::KIND_MERCHANT,
        self::KIND_KING,
        self::KIND_CONDOTIERRE,
        self::KIND_PRIEST,
    ];

    /**
     * @ORM\Column(type="string")
     */
    private string $name;

    /**
     * @ORM\Column(type="integer")
     */
    private int $cost;

    /**
     * @ORM\Column(type="string")
     */
    private string $kind;

    public function __construct(string $name, int $cost, string $kind)
    {
        $this->name = $name;
        $this->cost = $cost;

        if (!in_array($kind, self::ALLOWED_KINDS)) {
            throw new Exception('wrong kind');
        }

        $this->kind = $kind;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getCost(): int
    {
        return $this->cost;
    }

    public function getKind(): string
    {
        return $this->kind;
    }
}