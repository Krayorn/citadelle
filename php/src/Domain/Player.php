<?php

declare(strict_types=1);

namespace App\Domain;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\UuidInterface;

/**
 * @ORM\Entity()
 * @ORM\Table(name="players")
 */
class Player
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="integer", unique=true)
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private int $id;

    /**
     * @ORM\Column(type="uuid")
     */
    private UuidInterface $gameId;

    /**
     * @ORM\Column(type="integer")
     */
    private int $golds;

    /**
     * One Player has many Cards.
     * @ORM\OneToMany(targetEntity="PlayerCard", cascade={"persist"}, indexBy="position", mappedBy="player")
     */
    private Collection $hand;

    /**
     * @ORM\Column(type="integer")
     */
    private bool $crowned;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private ?string $character;

    public function __construct(UuidInterface $gameId, int $id, bool $crowned)
    {
        $this->gameId = $gameId;
        $this->id = $id;
        $this->golds = 0;
        $this->hand = new ArrayCollection();
        $this->crowned = $crowned;
        $this->character = null;
    }

    public function earnGold(int $amount)
    {
        $this->golds += $amount;
    }

    public function drawCard(Deck $deck)
    {
        $this->hand->add(new PlayerCard($this, $deck->getNextCard())); ;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function isCrowned(): bool
    {
        return $this->crowned;
    }

    public function getCharacter(): ?string
    {
        return $this->character;
    }
}