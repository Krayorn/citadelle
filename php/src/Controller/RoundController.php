<?php

declare(strict_types=1);

namespace App\Controller;

use App\Action\InitializeFirstRound;
use App\Repository\RoundRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Messenger\Stamp\HandledStamp;
use Symfony\Component\Routing\Annotation\Route;

class RoundController
{
    private MessageBusInterface $commandBus;
    private RoundRepository $roundRepository;

    public function __construct(
        MessageBusInterface $commandBus,
        RoundRepository $roundRepository
    )
    {
        $this->commandBus = $commandBus;
        $this->roundRepository = $roundRepository;
    }

    /**
     * @Route("/newGame", methods={"POST"}, name="create_game")
     */
    public function createGame(Request $request): JsonResponse
    {
        $envelope = $this->commandBus->dispatch(new InitializeFirstRound());
        $handledStamp = $envelope->last(HandledStamp::class);

        return new JsonResponse('', JsonResponse::HTTP_OK);
    }

    /**
     * @Route("round/{roundId}/pick/{playerId}", methods={"GET"}, name="get_available_roles")
     */
    public function getAvailableRoles(Request $request, int $roundId, int $playerId): JsonResponse
    {
        $round = $this->roundRepository->findById($roundId);

        if (!$round->canPlayerPick($playerId)) {
            return new JsonResponse('Can\'t pick.', JsonResponse::HTTP_CONFLICT);
        }

        return new JsonResponse([
            'available' => $round->getAvailableCharacters(),
            'visibleDiscarded' => $round->getVisibleDiscardedCharacters(),
        ], JsonResponse::HTTP_OK);
    }
}
